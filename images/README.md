Build image snippet

```

 func buildImage(){
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        var attribute = [ NSAttributedString.Key.foregroundColor: UIColor.white,
                          NSAttributedString.Key.font: UIFont(name: "Arial", size: 50.0)!,
                          .paragraphStyle: paragraph]
        (1...100).forEach{ index in
            let destination = docDirectoryUrl().appendingPathComponent("\(index).jpg")
            let image = text(text: "\(index)", textAttributes: attribute, bckColor: .systemGray, size: CGSize(width: 200, height: 200))
            _ = saveImageJPEG(image: image, compression: 0.9, to: destination)
        }
        print("open \(docDirectoryPath())")
    }
    
    /// Application document path.
    ///
    /// - Returns: path
    private func docDirectoryPath() -> String{
        let dirPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory,
                                                           .userDomainMask,
                                                           true)
        return dirPaths[0]
    }
    
    /// Application document url.
    ///
    /// - Returns: url
    private func docDirectoryUrl() -> URL {
        return URL(fileURLWithPath: docDirectoryPath())
    }
        
    /// Create a file with data at url.
    /// If operation is succesfull, returns true.
    /// Otherwise returns false.
    /// - Parameter data : the data to write in the file.
    /// - Parameter to : create file at url.
    /// - returns: Operation status.
    private func createFile(data:Data, to url:URL)->Bool{
        var status = true
        do{
            try data.write(to: url)
        }
        catch{
            print("Unexpected error : \(error.localizedDescription)")
            status = false
        }
        return status
    }
    
    /**
     Save an UIImage as a jpeg file.
     - Parameter image : the image in memory to save.
     - Parameter compression : the compression ratio (0.0->1.0)
     - Parameter to : url
     - returns : true if operation ends successfully, false otherwise.
     */
    private func saveImageJPEG(image:UIImage, compression:CGFloat, to:URL)->Bool{
        //Save it
        if let data =  image.jpegData(compressionQuality: compression) {
            return createFile(data: data, to: to)
        }
        else{
            print("Cannot create JPEG from UIImage")
            return false
        }
    }
    
    /**
     Generate an UIImage that includes text.
     ```
     let textFontAttributes = AttributeStringDictHelper.create(font: UIFont(name: "Thonburi-Bold",
     size: 40)!,
     fontColor: UIColor.white,
     backgrounColor: UIColor.clear,
     alignement: .center)
     
     let image = UIImage.text(text: "test",
     textAttributes: textFontAttributes,
     bckColor: UIColor.blue,
     size: CGSize(width:160,height:50))
     ```
     - parameter text : text.
     - parameter textAttributes : attributes.
     - parameter bckColor : Background color.
     - parameter size : Image size.
     */
    func text(text: String,
                     textAttributes: [NSAttributedString.Key:Any],
                     bckColor: UIColor,
                     size: CGSize) -> UIImage {
        let rect = CGRect(origin: CGPoint.zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(bckColor.cgColor)
        context?.fill(rect)
        text.draw(in: rect, withAttributes: textAttributes as [NSAttributedString.Key:Any])
        let outputImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return outputImage ?? UIImage()
    }
```